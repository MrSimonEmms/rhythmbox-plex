PROJECT ?= rhythmbox-plex

dev:
	mkdir -p ${HOME}/.local/share/rhythmbox/plugins
	ln -sfn ${PWD} ${HOME}/.local/share/rhythmbox/plugins/${PROJECT}

	rhythmbox --debug-match PlexPlugin
.PHONY: dev
